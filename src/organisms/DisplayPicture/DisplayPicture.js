import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Image from '../../atoms/Image';
import Upload from '../../atoms/Upload';
import Modal from '../../molecules/Modal';
import styles from './styles.module.scss'
import Avatar from '../../assets/avatar.png';

class DisplayPicture extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showOverlay: false,
      showEditModal: false,
      previewUrl: '',
    };
  }

  componentDidUpdate(prevProps) {
    const { loading: prevLoading } = prevProps;
    const { loading } = this.props;
    if (prevLoading && prevLoading !== loading) {
      this.handleClose();
    }
  }

  handleMouseOver = () => {
    this.setState({
      showOverlay: true,
    });
  }

  handleMouseLeave = () => {
    this.setState({
      showOverlay: false,
    });
  }

  handleEdit = () => {
    this.setState({
      showEditModal: true,
      showOverlay: false,
    });
  }

  handleClose = () => {
    this.setState({
      showEditModal: false,
      previewUrl: '',
    });
  }

  handleUpload = (e) => {
    const { onUpload } = this.props;
    onUpload(e).then((previewUrl) => {
      console.log(previewUrl);
      this.setState({ previewUrl });
    }).then(() => { });
  }

  _renderModalBody = () => {
    const { previewUrl } = this.state;
    const { src, uploading } = this.props;
    const url = previewUrl || src;
    return (
      <div className={styles.dpModal}>
        <Image
          className={styles.dpPreviewImage}
          src={url}
        />
        <div className={styles.uploadBtn}>
          <Upload
            label="Upload"
            onClick={this.handleUpload}
            loading={uploading}
          />
        </div>
      </div>
    );
  }

  render() {
    const { src, uploading, onSave, loading } = this.props;
    const { showOverlay, showEditModal, previewUrl } = this.state;
    const opacity = showOverlay ? 1 : 0;
    const disabled = uploading || !(previewUrl && previewUrl !== src);
    return (
      <div className={styles.dpContainer}>
        <Image
          fallback=""
          className={styles.dpImage}
          src={src}
        />
        <div
          onMouseOver={this.handleMouseOver}
          onMouseLeave={this.handleMouseLeave}
          className={styles.overlay} style={{ opacity }}
        >
          <div
            className={styles.dpImageMaskInfo}
            onClick={this.handleEdit}
          >
            <i className="bi bi-pencil-fill" /> Edit
          </div>
        </div>
        <Modal
          show={showEditModal}
          onCancel={this.handleClose}
          title="Change profile photo"
          body={this._renderModalBody()}
          onOk={() => onSave(previewUrl)}
          loading={loading}
          okBtnProps={{ disabled }}
          cancelBtnProps={{ disabled }}
        />
      </div>
    );
  }
};

DisplayPicture.defaultProps = {
  src: Avatar,
  uploading: false,
  loading: false,
};

DisplayPicture.propTypes = {
  onSave: PropTypes.func.isRequired,
  onUpload: PropTypes.any.isRequired,
  src: PropTypes.string,
  uploading: PropTypes.bool,
  loading: PropTypes.bool,
};

export default DisplayPicture;
