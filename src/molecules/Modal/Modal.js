import React from 'react';
import PropTypes from 'prop-types';
import { Modal as Popup } from 'react-bootstrap';
import Button from '../../atoms/Button';

const Modal = (props) => {
  const {
    show,
    title,
    body,
    className,
    showFooter,
    okText,
    cancelText,
    okBtnProps,
    cancelBtnProps,
    loading,
    onCancel,
    onOk,
    closable,
  } = props;
  const { disabled: okDisabled } = okBtnProps;
  const { disabled: cancelDisabled } = cancelBtnProps;
  return (
    <Popup
      show={show}
      className={className}
      onHide={onCancel}
      backdrop="static"
      keyboard={false}
    >
      <Popup.Header
        closeButton={closable}>
        <Popup.Title>
          {title}
        </Popup.Title>
      </Popup.Header>
      <Popup.Body>
        {body}
      </Popup.Body>
      {showFooter && (
        <Popup.Footer>
          {cancelText && (
            <Button
              type="default"
              label={cancelText}
              onClick={onCancel}
              disabled={cancelDisabled || loading}
            />)}
          {okText && (
            <Button
              type="primary"
              label={okText}
              onClick={onOk}
              disabled={okDisabled}
              loading={loading}
            />)}
        </Popup.Footer>
      )}
    </Popup>
  );
};

Modal.defaultProps = {
  show: true,
  title: '',
  className: '',
  showFooter: true,
  okText: 'Ok',
  cancelText: 'Cancel',
  okBtnProps: {},
  cancelBtnProps: {},
  loading: false,
  closable: true,
};

Modal.propTypes = {
  onCancel: PropTypes.func.isRequired,
  body: PropTypes.any.isRequired,
  show: PropTypes.bool,
  title: PropTypes.string,
  className: PropTypes.string,
  showFooter: PropTypes.bool,
  okText: PropTypes.string,
  cancelText: PropTypes.string,
  onOk: PropTypes.func,
  okBtnProps: PropTypes.object,
  cancelBtnProps: PropTypes.object,
  loading: PropTypes.bool,
  closable: PropTypes.bool,
};

export default Modal;
