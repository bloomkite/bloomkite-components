import React from 'react';
import PropTypes from 'prop-types';
import { Image as Img } from 'react-bootstrap';
import cx from 'classnames';
import styles from './styles.module.scss';

const Image = (props) => {
  const { alt, src, className = '' } = props;
  return (
    <Img
      className={cx(styles.bloomkiteImg, className)}
      alt={alt}
      src={src}
    />
  );
};

Image.defaultProps = {
  className: '',
  alt: '',
};

Image.propTypes = {
  src: PropTypes.string.isRequired,
  className: PropTypes.string,
  alt: PropTypes.string,
};

export default Image;
