import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './styles.module.scss';

const WithErrors = (WrappedComponent) => {
  return class extends React.Component {
    render() {
      const { message, className, ...rest } = this.props;
      return (
        <div className={cx(styles.errorWrapper, className)}>
          <WrappedComponent {...rest} />
          {message && (
            <div
              className={styles.message}
            >
              {message}
            </div>
          )}
        </div>
      );
    }
  }
};

WithErrors.defaultProps = {
  message: '',
};

WithErrors.propTypes = {
  message: PropTypes.string,
};

export default WithErrors;
