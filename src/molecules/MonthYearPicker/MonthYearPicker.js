import React from 'react';
import ReactDatePicker from 'react-datepicker';
import moment from 'moment';

const numberRange = (start, end) => new Array(end - start).fill().map((d, i) => i + start);

const MonthYearPicker = ({
  input, label, id, inputClass, config = {}, allValues, rowIndex,
}) => {
  const format = config.format || 'MMM-YYYY';
  const value = input.value ? moment(input.value).format(format) : null;
  const d = new Date();
  const years = numberRange(1950, d.getFullYear() + 1);
  let minDate = null;
  if (label && label.includes('To Year')) {
    minDate = new Date(allValues[rowIndex].fromYear);
  }
  return (
    <div className={inputClass}>
      {label && <label htmlFor={id} dangerouslySetInnerHTML={{ __html: label }} />}
      <ReactDatePicker
        renderCustomHeader={({ date, changeYear }) => (
          <div
            style={{
              margin: 10,
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <select value={new Date(date).getFullYear()} onChange={({ target: { value } }) => changeYear(value)}>
              {years.map((option) => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>
          </div>
        )}
        id={id}
        value={value}
        onChange={input.onChange}
        autoComplete="off"
        peekNextMonth
        showMonthDropdown
        showYearDropdown
        dropdownMode="select"
        minDate={minDate}
        maxDate={new Date()}
        selected={value ? new Date(value) : null}
        {...config}
      />
    </div>
  );
};

export default MonthYearPicker;
