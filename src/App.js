import { useState } from "react";
import classNames from "classnames";
import "./App.css";
import Button from "./atoms/Button";
import DatePicker from "./molecules/DatePicker";
import DisplayPicture from "./organisms/DisplayPicture";
import MonthYearPicker from "./molecules/MonthYearPicker";
import "./index.css";

function App() {
  const [sampleDate, setSampleDate] = useState(new Date());
  const [sampleMonthYear, setSampleMonthYear] = useState(new Date());
  return (
    <div className="App">
      <Button label="Add Project" />
      <DatePicker
        displayFormat="dd-MM-yyyy"
        onChange={(day) => {
          setSampleDate(day);
        }}
        showOnInputClick
        date={sampleDate}
        maxDate={new Date()}
        placeholder="Select Date"
        className=""
        disabled={false}
        readOnly={true}
      />
      <MonthYearPicker
        inputClass={classNames("bloomkite-date-picker")}
        input={{
          value: sampleMonthYear,
          onChange: setSampleMonthYear,
        }}
        config={{
          showMonthYearPicker: true,
          format: "MMM-YYYY",
        }}
      />
      <div style={{ height: 80 }}>
        <DisplayPicture />
      </div>
    </div>
  );
}

export default App;
