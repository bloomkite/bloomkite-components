import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './styles.module.scss';

const Checkbox = (props) => {
  const { label, checked, onChange, className, disabled } = props;
  return (
    <label className={cx(styles.checkboxWrapper, className, { [styles.checkboxDisabled]: disabled })}>
      <span className={cx(styles.checkbox, { [styles.checkboxChecked]: checked })}>
        <input
          type="checkbox"
          className={styles.checkboxInput}
          checked={checked}
          onChange={onChange}
          disabled={disabled}
        />
        <span className={styles.checkboxInner} />
      </span>
      {label && <span>{label}</span>}
    </label >
  );
};

Checkbox.defaultProps = {
  className: '',
};

Checkbox.propTypes = {
  label: PropTypes.string,
  onChange: PropTypes.func,
  className: PropTypes.string,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
};

export default Checkbox;
