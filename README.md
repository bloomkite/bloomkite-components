# Bloomkite Components

Bloomkite Components is a React UI Library, Which contains pre-defined components based on organization.

## 📦 Install

### package.json

```bash
 "bloomkite-components": "git+https://<username>@bitbucket.org/bloomkite/bloomkite-components.git#develop"
```

### webpack.config.js

```bash
    resolve: {
        extensions: ['.js', '.jsx', '.sass'],
        alias: {
            bcomponents: 'bloomkite-components/src'
        },
    },
```

Make sure you have used below loader in your project. 

Note: for Production mode, use `MiniCssExtractPlugin`

```jsx
const cssRegex = /\.css$/;
const cssModuleRegex = /\.module.css$/;
const sassRegex = /\.(sass|scss)$/;
const sassModuleRegex = /\.module.(sass|scss)$/;
```

```jsx
    {
        test: cssRegex,
        exclude: cssModuleRegex,
        use: ['style-loader', 'css-loader'],
    },
    {
        test: cssModuleRegex,
        use: [
            'style-loader',
            {
                loader: 'css-loader',
                options: {
                importLoaders: 1,
                modules: true,
                },
        }],
    },
    {
        test: sassRegex,
        exclude: sassModuleRegex,
        use: ['style-loader', 'css-loader', 'sass-loader'],
    },
    {
        test: sassModuleRegex,
        use: [
            'style-loader',
            {
                loader: 'css-loader',
                options: {
                modules: true,
            },
            },
            'sass-loader',
        ],
    },
```

## 🔨 Usage

```jsx
import Button from 'bcomponents/atoms/Button';

    const App = () => (
    <>
        <Button type="primary">PRESS ME</Button>
    </>
    );
```

## ⌨️ Development

```bash
$ git clone https://<username>@bitbucket.org/bloomkite/bloomkite-components.git
$ cd bloomkite-components
$ npm install
$ npm start
```

