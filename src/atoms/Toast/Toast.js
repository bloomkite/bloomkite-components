import React from 'react';
import PropTypes from 'prop-types';
import { ToastContainer, toast } from 'react-toastify';

const TOAST_TYPES = {
  SUCCESS: 'success',
  INFO: 'info',
  ERROR: 'error',
  DEFAULT: 'default',
};

export const toastr = (type, content, position, delay) => {
  let fn = null;
  switch (type) {
    case TOAST_TYPES.SUCCESS:
      fn = toast.success;
      break;
    case TOAST_TYPES.INFO:
      fn = toast.info;
      break;
    case TOAST_TYPES.ERROR:
      fn = toast.info;
      break;
    default:
      fn = toast;
      break;
  }
  fn(content, {
    position,
    autoClose: delay,
  });
};

const ToastWrapper = (props) => (
  <ToastContainer
    position="top-right"
    autoClose={5000}
    hideProgressBar={false}
    newestOnTop={false}
    closeOnClick
    rtl={false}
    pauseOnFocusLoss
    draggable
    pauseOnHover
  />
);

ToastWrapper.defaultProps = {
  position: 'top-right',
};

ToastWrapper.propTypes = {
  position: PropTypes.string,
};

export default ToastWrapper;
