import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import ReactSelect from 'react-select';
import styles from './styles.module.scss';

const customStyles = (props) => {
  const { style: { theme } } = props;
  const baseColor = '#d9d9d9';
  return {
    option: (provided, state) => ({
      ...provided,
      borderBottom: `1px solid ${theme || baseColor} !important`,
      backgroundColor: (state.isSelected || state.isFocused) && theme ? theme : '#fff',
      fontSize: 13,
      ':active': {
        ...provided[':active'],
        backgroundColor: theme || baseColor,
      },
    }),
    control: (base, state) => ({
      ...base,
      margin: 0,
      width: '100%',
      minWidth: 0,
      color: '#000000d9',
      fontSize: '14px',
      backgroundColor: '#fff',
      borderColor: state.isFocused ? `${theme || baseColor}` : baseColor,
      boxShadow: state.isFocused ? `0 0 0 1px ${theme ? theme : baseColor}` : 'none',
      borderRadius: '2px',
      transition: 'all .3s',
      minHeight: '32px',
      ':hover': {
        ...base[':hover'],
        borderColor: theme || baseColor,
      },
    }),
    dropdownIndicator: (base) => ({
      ...base,
      padding: '4px'
    }),
  }
};

const Select = (props) => {
  const { value, className, onChange, options, isMulti, name, maxLength, ...rest } = props;
  const handleChange = (values) => {
    if (isMulti && maxLength) {
      if (values && values.length <= maxLength) {
        onChange(values);
      }
      return;
    }
    onChange(values);
  };
  return (
    <div className={cx(styles.selectWrapper, className)}>
      <ReactSelect
        styles={customStyles(props)}
        className={styles.selectContainer}
        value={value}
        onChange={handleChange}
        options={options}
        isMulti={isMulti}
        name={name}
        {...rest}
      />
    </div>
  );
};

Select.defaultProps = {
  className: '',
  placeholder: '',
  name: '',
  style: {},
  isMulti: false,
};

Select.propTypes = {
  options: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.string.isRequired,
  style: PropTypes.object,
  className: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  isMulti: PropTypes.bool,
  maxLength: PropTypes.number,
};

export default Select;
