import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './styles.module.scss';

const TextInput = (props) => {
  const {
    value,
    type,
    onChange,
    placeholder,
    className,
    allowClear,
    maxLength,
    name,
    ...rest
  } = props;

  const inputRef = useRef(null);

  const handleClear = () => {
    const event = { target: { name, value: '' } };
    onChange(event);
    inputRef.current.focus();
  };

  const enableClear = allowClear && value;
  return (
    <div className={cx(styles.textInputWrapper, className, { [styles.allowClear]: enableClear })}>
      <input
        ref={inputRef}
        className={cx(styles.textInput)}
        placeholder={placeholder}
        onChange={onChange}
        type={type}
        value={value}
        maxLength={maxLength}
        name={name}
        {...rest}
      />
      {enableClear && (
        <span
          id="close-btn"
          className={styles.closeIcon}
          onClick={handleClear}
          value=""
        >
          &times;
        </span>
      )}
    </div>
  );
};

TextInput.defaultProps = {
  className: '',
  placeholder: '',
  type: 'text',
  allowClear: true,
  maxLength: null,
  name: '',
};

TextInput.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.string.isRequired,
  className: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  allowClear: PropTypes.bool,
  maxLength: PropTypes.number,
  name: PropTypes.string,
};

export default TextInput;
