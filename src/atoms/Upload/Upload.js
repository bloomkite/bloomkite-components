import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Button from '../../atoms/Button';
import styles from './styles.module.scss';

const Upload = (props) => {
  const { loading, label, onClick, accept, className, multiple } = props;
  const inputEl = useRef(null);
  const handleUpload = (e) => {
    e.preventDefault();
    inputEl.current.click();
  };

  const onUploadClick = (e) => {
    e.target.value = null;
  };

  const onDropFile = (e) => {
    const { target: { files } } = e;
    onClick(files);
  };

  return (
    <div className={cx(styles.uploadBtn, className)}>
      <Button
        type="primary"
        label={label}
        onClick={handleUpload}
        loading={loading}
        icon={<i class="bi bi-upload" />}
      />
      <input
        className={styles.uploadInput}
        type="file"
        ref={inputEl}
        name="file-input"
        multiple={multiple}
        onChange={onDropFile}
        onClick={onUploadClick}
        accept={accept}
      />
    </div>
  );
};

Upload.defaultProps = {
  loading: false,
  className: '',
  accept: '',
  multiple: false,
};

Upload.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  className: PropTypes.string,
  accept: PropTypes.string,
  multiple: PropTypes.bool,
};

export default Upload;
