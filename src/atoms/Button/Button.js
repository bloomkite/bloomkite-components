import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Spinner } from 'react-bootstrap';
import styles from './styles.module.scss';

const BUTTON_TYPES = {
  PRIMARY: 'primary',
  LINK: 'link',
  DEFAULT: 'default',
};

const Button = (props) => {
  const { label, type, onClick, disabled, loading, icon, className } = props;
  const btnClasses = cx(
    styles.bloomkiteBtn,
    className,
    {
      [styles.primaryBtn]: type === BUTTON_TYPES.PRIMARY,
      [styles.linkBtn]: type === BUTTON_TYPES.LINK,
      [styles.btnLoading]: loading,
    });
  return (
    <button
      onClick={!loading ? onClick : null}
      className={btnClasses}
      disabled={disabled || loading}
    >
      {loading && (
        <Spinner
          className={styles.loader}
          animation="border"
        />)}
      {icon && (
        <div className={styles.btnIcon}>
          {icon}
        </div>
      )}
      {label}
    </button>
  );
};

Button.defaultProps = {
  type: BUTTON_TYPES.PRIMARY,
  loading: false,
  className: '',
  disabled: false,
};

Button.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  type: PropTypes.string,
  loading: PropTypes.bool,
  icon: PropTypes.any,
  className: PropTypes.string,
  disabled: PropTypes.bool,
};

export default Button;
