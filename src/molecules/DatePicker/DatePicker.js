import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/src/stylesheets/variables.scss';
import 'react-datepicker/src/stylesheets/datepicker.scss';
import 'react-datepicker/src/stylesheets/datepicker-cssmodules.scss';
import styles from './styles.module.scss'

const CustomDatePicker = ({ date, onChange, mode, maxDate, dateFormat, placeholder, disabled, className }) => {
    return (
        <DatePicker
            className={className}
            dateFormat={dateFormat || 'dd/MM/yyyy'}
            maxDate={maxDate || moment().toDate()}
            selected={date}
            onChange={(date) => onChange(date)}
            peekNextMonth
            showMonthDropdown
            showYearDropdown
            dropdownMode="select"
            mode={mode}
            closeOnSelect={true}
            disabled={disabled || false}
            disabledKeyboardNavigation={disabled || false}
            placeholderText={placeholder || 'Select Date'}
        />
    );
};

export default CustomDatePicker;
